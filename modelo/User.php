<?php
require_once('Tienda.php');
require_once('Facturas.php');
class User {
    protected $id;
    protected $nombre;
    protected $apellido1;
    protected $apellido2;
    protected $rol; 
    protected $tlf;
    protected $email;
    protected $direccion;
    protected $password;
    protected $fecha_sesion;


    public function __get($atributo) {
        return $this->$atributo;
    }
    public function __set($atributo,$valor) {
        $this->$atributo=$valor;
    }
    public function __construct($row) {
        $this->$id = $row['id'];
        $this->$nombre = $row['nombre'];
        $this->$apellido1 = $row['apellido1'];
        $this->$apellido2 = $row['apellido2'];
        $this->$rol = $row['rol'];
        $this->$tlf = $row['telefono'];
        $this->$email = $row['email'];
        $this->$direccion = $row['direccion'];
        $this->$password = $row['password'];
        $this->$fecha_sesion = $row['fecha_sesion'];

    } 

    public static function usuarioComprobado($username,$password){
        $sql = "SELECT * FROM Usuario WHERE email='$username'";
        $resultado = Tienda::ejecutaConsulta($sql);
        $flag = false;
        if($resultado) {
            $password = $resultado->fetch(PDO::FETCH_ASSOC);
                $flag = true;
        }
        return $flag;
    }

}