<?php
require_once("Tienda.php");
require_once("Facturas.php");
//require_once("CategoriaProducto.php");
class Categoria {
        protected $id;
        protected $nombre;
        protected $descripcion;
  
       
        
        public function __construct($row) {
            $this->id=$row['id'];
            $this->nombre=$row['nombre'];
            $this->descripcion=$row['descripcion'];
       

        }

        public function __get($atributo) {
            return $this->$atributo;
        }

        public function __set($atributo,$valor) {
            return $this->$atributo=$valor;
        }
        static function obtenerCategorias() {
            $sql = "SELECT * from categoriapesca";
            $resultado = Tienda::ejecutaConsulta($sql);
            $categorias = [];
          
            if($resultado) {
                while($row = $resultado->fetch(PDO::FETCH_ASSOC)) {
                    array_push($categorias, new Categoria($row));
                }
            }
            return $categorias;
         
        }
        

    }
?>