<?php
class DB {
    public static function mensajeError($mensaje) {
        echo "<p><span style='color: red; font-size: 18pt;'>$mensaje</span></p>";
    }
    public static function ejecutaConsulta($sql) {
        $dsn = "mysql:host=localhost;dbname=dbs7211298;charset=utf8"; 
        $usuario = 'root';
        $contrasena = '';
        
        try {
            $dwes = new PDO($dsn, $usuario, $contrasena);
      
            if (substr(strtoupper(trim($sql)),0,6)=="SELECT") {
                $resultado = $dwes->query($sql);
               
            } else {
                $resultado = $dwes->exec($sql);
          
            }
        } catch (PDOException $ex) {
            if (!isset($dwes)) {
                self::mensajeError("Error conectando con el servidor de bases de datos.");
                exit(1);
            }
            self::mensajeError($ex->getMessage());
            exit(2);
        }
        return $resultado;
    }
}
