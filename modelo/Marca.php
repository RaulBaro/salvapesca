<?php
require_once("Tienda.php");
require_once("Facturas.php");
class Marca {
    protected $id;
    protected $nombre;
    protected $pais_origen;

   
    
    public function __construct($row) {
        $this->id=$row['id'];
        $this->nombre=$row['nombre'];
        $this->pais_origen=$row['pais_origen'];

    }

    public function __get($atributo) {
        return $this->$atributo;
    }

    public function __set($atributo,$valor) {
        return $this->$atributo=$valor;
    }
    static function obtenerMarcas() {
        $sql = "SELECT * from marcapesca";
        $resultado = Tienda::ejecutaConsulta($sql);
        $marcas = [];
      
        if($resultado) {
            while($row = $resultado->fetch(PDO::FETCH_ASSOC)) {
                array_push($marcas, new Marca($row));
            }
        }
        return $marcas;
     
    }
}

?>